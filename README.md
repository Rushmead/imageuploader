# ImageUploader project

This project uses Quarkus to allow the uploading of images through a webpage.

For convenience a compiled native version of the application has been included in this repository.

## Running the application

This application using Docker compose to run. Execute the following command to start it

```
docker-compose up -d
```

It will then be available at http://localhost:8081