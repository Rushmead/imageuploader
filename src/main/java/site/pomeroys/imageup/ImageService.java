package site.pomeroys.imageup;

import org.jboss.logging.Logger;
import site.pomeroys.imageup.objects.Image;
import site.pomeroys.imageup.objects.ImageUploadData;

import javax.enterprise.context.ApplicationScoped;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.UUID;

/**
 * A service that retrieves and stores images from the database.
 */
@ApplicationScoped
public class ImageService {


    // Logger used for debugging
    private static final Logger LOG = Logger.getLogger(ImageService.class);

    /**
     * Generates a random ID using a UUID.
     * @return A six character string
     */
    private String generateID(){
        return UUID.randomUUID().toString().substring(30, 36).toUpperCase();
    }

    /**
     * Converts the file to a byte array and stores it in the database
     * @param data          The data from the request
     * @return              The Image that was stored in the database
     * @throws Exception    If a part of the process fails, it will throw this exception.
     *                      This should be returned to the client.
     */
    public Image processAndCreateImage(ImageUploadData data) throws Exception {
        return newImage(processImage(data.file), data.fileType);
    }

    /**
     * Takes the byte array and image type and saves it to the database
     * @param bytes         The bytes from the image.
     * @param imageType     The type of image
     * @return              The Image instance that was saved to the database.
     */
    public Image newImage(byte[] bytes, String imageType){
        Image newImage = new Image(generateID(), bytes, imageType);
        newImage.persist();
        return newImage;
    }

    /**
     * Takes the provided file and converts it into a byte array
     * @param file          The file that was uploaded
     * @return              An array of bytes from the image.
     * @throws Exception    If the file is not found, it will throw an exception.
     */
    public byte[] processImage(File file) throws Exception{
        byte[] bytes;
        try {
            bytes = Files.readAllBytes(file.toPath());
        } catch(IOException ignored){
            throw new Exception("failed to load file");
        }
        return bytes;
    }

    /**
     * Find an image by the ID
     * @param id    The ID to look for
     * @return      An Image instance or null if it was not found.
     */
    public Image getImage(String id){
        return Image.findById(id);
    }
}
