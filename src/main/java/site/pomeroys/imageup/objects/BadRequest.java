package site.pomeroys.imageup.objects;

import io.quarkus.runtime.annotations.RegisterForReflection;

/**
 * An Object wrapper for a bad request to be returned to the client.
 */
@RegisterForReflection
public class BadRequest {

    public boolean success = false;
    public String message;

    public BadRequest(String message) {
        this.message = message;
    }
}
