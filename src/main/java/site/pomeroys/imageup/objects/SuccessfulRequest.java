package site.pomeroys.imageup.objects;

import io.quarkus.runtime.annotations.RegisterForReflection;

@RegisterForReflection
public class SuccessfulRequest {

    public boolean success = true;
    public String message;

    public SuccessfulRequest(String message){
        this.message = message;
    }

}
