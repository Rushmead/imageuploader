package site.pomeroys.imageup.objects;

import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.PanacheMongoEntityBase;
import io.quarkus.runtime.annotations.RegisterForReflection;
import org.bson.codecs.pojo.annotations.BsonId;

/**
 * An Object wrapper for the Image collection
 * Utilises Panache and POJO.
 */
@MongoEntity(collection="images")
@RegisterForReflection
public class Image extends PanacheMongoEntityBase {

    @BsonId
    public String imageID;
    public byte[] image;
    public String imageType;

    public Image(){}

    public Image(String id, byte[] bytes, String type){
        this.imageID = id;
        this.image = bytes;
        this.imageType = type;
    }

    public String getImageID() {
        return imageID;
    }

    public byte[] getImage() {
        return image;
    }

    public String getImageType() {
        return imageType;
    }
}
