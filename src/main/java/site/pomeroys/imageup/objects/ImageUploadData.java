package site.pomeroys.imageup.objects;

import javax.ws.rs.FormParam;
import java.io.File;

/**
 * Object wrapper for the multi-part form data
 */
public class ImageUploadData {
    /**
     * Image file
     */
    @FormParam("file")
    public File file;

    /**
     * Image file type
     */
    @FormParam("fileType")
    public String fileType;
}
