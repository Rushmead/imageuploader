package site.pomeroys.imageup;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jboss.logging.Logger;
import org.jboss.resteasy.annotations.jaxrs.PathParam;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;
import site.pomeroys.imageup.objects.BadRequest;
import site.pomeroys.imageup.objects.Image;
import site.pomeroys.imageup.objects.ImageUploadData;
import site.pomeroys.imageup.objects.SuccessfulRequest;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * The resource that provides routes for retrieving and creating images.
 */
@Path("/image")
public class ImageResource {

    @Inject
    ImageService service;

    // Max size of image in MB
    @ConfigProperty(name = "images.maxSize", defaultValue = "10")
    public int maxSize;

    private static final Logger LOG = Logger.getLogger(ImageResource.class);

    @GET
    @Path("/{id}")
    @Produces(MediaType.MEDIA_TYPE_WILDCARD)
    public Response getImage(@PathParam String id) {
        Image image = service.getImage(id);
        if(image == null){
            return Response.status(404).build();
        }
        return Response.status(200).type(image.getImageType()).entity(image.getImage()).build();
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public Response uploadImage(@MultipartForm ImageUploadData data){
        if(data.file == null){
            return Response.status(Response.Status.BAD_REQUEST).entity(new BadRequest("Missing file")).build();
        }
        if(data.fileType == null || data.fileType.length() < 1){
            return Response.status(Response.Status.BAD_REQUEST).entity(new BadRequest("Missing file type")).build();
        }
        if(!data.fileType.contains("image/")){
            return Response.status(Response.Status.BAD_REQUEST).entity(new BadRequest("Invalid file type")).build();
        }
        if(data.file.length() > ((maxSize * 1000) * 1000)){
            return Response.status(Response.Status.BAD_REQUEST).entity(new BadRequest("Image too big! Max size is " + maxSize + "MB")).build();
        }
        try {
            Image image = service.processAndCreateImage(data);
            String newImageID = image.getImageID();
            LOG.info("Successfully created image " + newImageID);
            return Response.status(200).entity(new SuccessfulRequest(newImageID)).build();
        } catch(Exception e) {
            LOG.error(e);
            return Response.status(500).entity(e.getMessage()).build();
        }
    }
}