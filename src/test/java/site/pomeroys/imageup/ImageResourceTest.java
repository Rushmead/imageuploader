package site.pomeroys.imageup;

import io.quarkus.test.common.PathTestHelper;
import io.quarkus.test.common.TestResourceManager;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import java.io.File;
import java.io.IOException;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.junit.jupiter.api.Test;
import site.pomeroys.imageup.objects.ImageUploadData;
import site.pomeroys.imageup.objects.SuccessfulRequest;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
public class ImageResourceTest {

    @Test
    public void testUploadingFile() throws IOException {
        File tmpFile = File.createTempFile("imageuploader", "test");
        tmpFile.deleteOnExit();
        given()
            .multiPart("file", tmpFile)
            .multiPart("fileType", "image/jpg")
          .when()
            .post("/image")
          .then()
             .statusCode(200);
    }

    @Test
    public void testUploadingFileWithNoFile(){
        given()
            .multiPart("fileType", "image/jpg")
        .when()
            .post("image")
        .then()
            .statusCode(400);
    }

    @Test
    public void testUploadingNonImage() throws IOException{
        File tmpFile = File.createTempFile("imageuploader", "test");
        tmpFile.deleteOnExit();
        given()
            .multiPart("file", tmpFile)
            .multiPart("fileType", "application/zip")
        .when()
            .post("image")
        .then()
            .statusCode(400);
    }

    @Test
    public void testGettingImageSucceeds() throws IOException {
        File tmpFile = File.createTempFile("imageuploader", "test");
        tmpFile.deleteOnExit();
        Response response = given()
            .multiPart("file", tmpFile)
            .multiPart("fileType", "image/jpg")
        .when()
            .post("image");
        ResponseBody body = response.getBody();
        response.then()
            .statusCode(200);
        String imageID = body.jsonPath().getString("message");
        given().when().get("/image/" + imageID).then().statusCode(200);
    }

    @Test
    public void testGettingInvalidImageFails(){
        given()
            .when()
            .get("/image/test123")
            .then()
            .statusCode(404);
    }

}